


  if &background == 'dark'

  let s:shade0 = "#0d0d14"
  let s:shade1 = "#292a2e"
  let s:shade2 = "#47474a"
  let s:shade3 = "#666668"
  let s:shade4 = "#878788"
  let s:shade5 = "#aaaaa9"
  let s:shade6 = "#cfcecc"
  let s:shade7 = "#f4f4ef"
  let s:accent0 = "#f95b88"
  let s:accent1 = "#f98f48"
  let s:accent2 = "#06b6e3"
  let s:accent3 = "#9fc249"
  let s:accent4 = "#d1ac34"
  let s:accent5 = "#068be2"
  let s:accent6 = "#06dcb4"
  let s:accent7 = "#c056b2"

  endif



  if &background == 'light'

  let s:shade0 = "#f4f4ef"
  let s:shade1 = "#cfcecc"
  let s:shade2 = "#aaaaa9"
  let s:shade3 = "#878788"
  let s:shade4 = "#666668"
  let s:shade5 = "#47474a"
  let s:shade6 = "#292a2e"
  let s:shade7 = "#0d0d14"
  let s:accent0 = "#cc6278"
  let s:accent1 = "#cc7c58"
  let s:accent2 = "#3390be"
  let s:accent3 = "#869859"
  let s:accent4 = "#a48b4f"
  let s:accent5 = "#337abc"
  let s:accent6 = "#33b18f"
  let s:accent7 = "#97608e"

  endif


  let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
  let s:p.normal.left = [ [ s:shade1, s:accent5 ], [ s:shade7, s:shade2 ] ]
  let s:p.normal.right = [ [ s:shade1, s:shade4 ], [ s:shade5, s:shade2 ] ]
  let s:p.inactive.right = [ [ s:shade1, s:shade3 ], [ s:shade3, s:shade1 ] ]
  let s:p.inactive.left =  [ [ s:shade4, s:shade1 ], [ s:shade3, s:shade0 ] ]
  let s:p.insert.left = [ [ s:shade1, s:accent3 ], [ s:shade7, s:shade2 ] ]
  let s:p.replace.left = [ [ s:shade1, s:accent1 ], [ s:shade7, s:shade2 ] ]
  let s:p.visual.left = [ [ s:shade1, s:accent6 ], [ s:shade7, s:shade2 ] ]
  let s:p.normal.middle = [ [ s:shade5, s:shade1 ] ]
  let s:p.inactive.middle = [ [ s:shade4, s:shade1 ] ]
  let s:p.tabline.left = [ [ s:shade6, s:shade2 ] ]
  let s:p.tabline.tabsel = [ [ s:shade6, s:shade0 ] ]
  let s:p.tabline.middle = [ [ s:shade2, s:shade4 ] ]
  let s:p.tabline.right = copy(s:p.normal.right)
  let s:p.normal.error = [ [ s:accent0, s:shade0 ] ]
  let s:p.normal.warning = [ [ s:accent2, s:shade1 ] ]

  let g:lightline#colorscheme#chronotype_hard#palette = lightline#colorscheme#fill(s:p)


