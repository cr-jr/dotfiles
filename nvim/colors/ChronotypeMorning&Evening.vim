if &background == 'dark'

let s:shade0 = "#191a28"
let s:shade1 = "#39313e"
let s:shade2 = "#594a54"
let s:shade3 = "#77666b"
let s:shade4 = "#958483"
let s:shade5 = "#b1a59f"
let s:shade6 = "#cdc6bd"
let s:shade7 = "#e9e8df"
let s:red = "#f95b88"
let s:orange = "#f98f48"
let s:aqua = "#06b6e3"
let s:green = "#9fc249"
let s:yellow = "#d1ac34"
let s:blue = "#068be2"
let s:teal = "#06dcb4"
let s:magenta = "#c056b2"

endif


if &background == 'light'

let s:shade0 = "#e9e8df"
let s:shade1 = "#cdc6bd"
let s:shade2 = "#b1a59f"
let s:shade3 = "#958483"
let s:shade4 = "#77666b"
let s:shade5 = "#594a54"
let s:shade6 = "#39313e"
let s:shade7 = "#191a28"
let s:red = "#cc6278"
let s:orange = "#cc7c58"
let s:aqua = "#3390be"
let s:green = "#869859"
let s:yellow = "#a48b4f"
let s:blue = "#337abc"
let s:teal = "#33b18f"
let s:magenta = "#97608e"

endif


highlight clear
syntax reset
let g:colors_name = "ChronotypeMorning&Evening"

""""""""""
" Normal "
""""""""""

exec "hi Normal guifg=".s:shade6." guibg=".s:shade0

"""""""""""""""""
" Syntax groups "
"""""""""""""""""

" The syntax highlight scheme chosen with the Alabaster Theme
" philosophy in mind. However...
"
" I added three additional categories:
" 1) syntax I don't want highlighted explicitly
" 2) backdrop general language keywords
" 3) idioms of a language
" =============================================================
" When background=dark
" 1. Backdrop: #77666b
" 2. Strings: #f98f48
" 3. Constants: #d1ac34
" 4. Comments: #f95b88
" 5. Globals: #9fc249
" 6. Idioms: #068be2
" 7. Negations: #cdc6bd
" =============================================================
" When background=light
" 1. Backdrop: #77666b
" 2. Strings: #97608e
" 3. Constants: #33b18f
" 4. Comments: #869859
" 5. Globals: #3390be
" 6. Idioms: #cc7c58
" 7. Negations: #39313e

if &background == 'dark'

  "Backdrop
  exec "hi Statement gui=none guifg=".s:shade4
  exec "hi Character gui=none guifg=".s:shade4
  exec "hi PreProc gui=none guifg=".s:shade4
  exec "hi htmlTag gui=none guifg=".s:shade4
  exec "hi cssNoise gui=none guifg=".s:shade4
  exec "hi cssBraces gui=none guifg=".s:shade4

  " Strings
  exec "hi String gui=none guifg=".s:orange

  " Constants
  exec "hi Constant gui=bold guifg=".s:yellow
  exec "hi jsNull gui=bold guifg=".s:yellow
  exec "hi jsUndefined gui=bold guifg=".s:yellow

  " Comments
  exec "hi Comment gui=none guifg=".s:red

  " Globals
  exec "hi Special gui=none guifg=".s:green
  exec "hi Function gui=none guifg=".s:green
  exec "hi htmlTagName gui=none guifg=".s:green
  exec "hi htmlSpecialTagName gui=none guifg=".s:green
  exec "hi cssTagName gui=none guifg=".s:green
  exec "hi jsExceptions gui=none guifg=".s:green
  exec "hi jsClassProperty gui=none guifg=".s:green

  " Idioms
  exec "hi Type gui=bold guifg=".s:blue
  exec "hi cssClassName gui=bold guifg=".s:blue
  exec "hi cssClassNameDot gui=bold guifg=".s:blue
  exec "hi cssIdentifier gui=bold guifg=".s:blue
  exec "hi jsThis gui=bold guifg=".s:blue
  exec "hi jsAsyncKeyword gui=bold guifg=".s:blue
  exec "hi jsForAwait gui=bold guifg=".s:blue
  exec "hi jsSuper gui=bold guifg=".s:blue

  " Negations
  exec "hi Operator gui=none guifg=".s:shade6
  exec "hi Identifier gui=none guifg=".s:shade6
  exec "hi htmlArg gui=none guifg=".s:shade6
  exec "hi htmlTitle gui=none guifg=".s:shade6
  exec "hi cssProp gui=none guifg=".s:shade6
  exec "hi jsFunction gui=none guifg=".s:shade6
  exec "hi StorageClass gui=none guifg=".s:shade6
  exec "hi xmlAttrib gui=none guifg=".s:shade6

endif

if &background == 'light'

  "Backdrop
  exec "hi Statement gui=none guifg=".s:shade4
  exec "hi Character gui=none guifg=".s:shade4
  exec "hi PreProc gui=none guifg=".s:shade4
  exec "hi htmlTag gui=none guifg=".s:shade4
  exec "hi cssNoise gui=none guifg=".s:shade4
  exec "hi cssBraces gui=none guifg=".s:shade4

  " Strings
  exec "hi String gui=none guifg=".s:magenta

  " Constants
  exec "hi Constant gui=bold guifg=".s:teal
  exec "hi jsNull gui=bold guifg=".s:teal
  exec "hi jsUndefined gui=bold guifg=".s:teal

  " Comments
  exec "hi Comment gui=none guifg=".s:green

  " Globals
  exec "hi Special gui=none guifg=".s:aqua
  exec "hi Function gui=none guifg=".s:aqua
  exec "hi htmlTagName gui=none guifg=".s:aqua
  exec "hi htmlSpecialTagName gui=none guifg=".s:aqua
  exec "hi cssTagName gui=none guifg=".s:aqua
  exec "hi jsExceptions gui=none guifg=".s:aqua
  exec "hi jsClassProperty gui=none guifg=".s:aqua

  " Idioms
  exec "hi Type gui=bold guifg=".s:orange
  exec "hi cssClassName gui=bold guifg=".s:orange
  exec "hi cssClassNameDot gui=bold guifg=".s:orange
  exec "hi cssIdentifier gui=bold guifg=".s:orange
  exec "hi jsThis gui=bold guifg=".s:orange
  exec "hi jsAsyncKeyword gui=bold guifg=".s:orange
  exec "hi jsForAwait gui=bold guifg=".s:orange
  exec "hi jsSuper gui=bold guifg=".s:orange

  " Negations
  exec "hi Operator gui=none guifg=".s:shade6
  exec "hi Identifier gui=none guifg=".s:shade6
  exec "hi htmlArg gui=none guifg=".s:shade6
  exec "hi htmlTitle gui=none guifg=".s:shade6
  exec "hi cssProp gui=none guifg=".s:shade6
  exec "hi jsFunction gui=none guifg=".s:shade6
  exec "hi StorageClass gui=none guifg=".s:shade6
  exec "hi xmlAttrib gui=none guifg=".s:shade6

endif


" GitGutter

exec "hi GitGutterAdd guifg=".s:green
exec "hi GitGutterChange guifg=".s:aqua
exec "hi GitGutterChangeDelete guifg=".s:aqua
exec "hi GitGutterDelete guifg=".s:red

" fugitive

exec "hi gitcommitComment guifg=".s:shade3
exec "hi gitcommitOnBranch guifg=".s:shade3
exec "hi gitcommitHeader guifg=".s:shade5
exec "hi gitcommitHead guifg=".s:shade3
exec "hi gitcommitSelectedType guifg=".s:green
exec "hi gitcommitSelectedFile guifg=".s:green
exec "hi gitcommitDiscardedType guifg=".s:aqua
exec "hi gitcommitDiscardedFile guifg=".s:aqua
exec "hi gitcommitUntrackedFile guifg=".s:red

"""""""""""""""""""""""
" Highlighting Groups "
"""""""""""""""""""""""

" Default

exec "hi ColorColumn guibg=".s:shade1
exec "hi Conceal guifg=".s:shade2
exec "hi Cursor guifg=".s:shade0
exec "hi CursorColumn guibg=".s:shade1
exec "hi CursorLine guibg=".s:shade1." cterm=none"
exec "hi Directory guifg=".s:blue
exec "hi DiffAdd guifg=".s:green." guibg=".s:shade1
exec "hi DiffChange guifg=".s:aqua." guibg=".s:shade1
exec "hi DiffDelete guifg=".s:red." guibg=".s:shade1
exec "hi DiffText guifg=".s:aqua." guibg=".s:shade2
exec "hi ErrorMsg guifg=".s:shade7." guibg=".s:red
exec "hi VertSplit guifg=".s:shade0." guibg=".s:shade3
exec "hi Folded guifg=".s:shade4." guibg=".s:shade1
exec "hi FoldColumn guifg=".s:shade4." guibg=".s:shade1
exec "hi SignColumn guibg=".s:shade0
exec "hi IncSearch guifg=".s:shade0." guibg=".s:aqua
exec "hi LineNr guifg=".s:shade2." guibg=".s:shade0
exec "hi CursorLineNr guifg=".s:shade3." guibg=".s:shade1
exec "hi MatchParen guibg=".s:shade2
exec "hi MoreMsg guifg=".s:shade0." guibg=".s:yellow
exec "hi NonText guifg=".s:shade2." guibg=".s:shade0
exec "hi Pmenu guifg=".s:shade6." guibg=".s:shade1
exec "hi PmenuSel guifg=".s:yellow." guibg=".s:shade1
exec "hi PmenuSbar guifg=".s:green." guibg=".s:shade1
exec "hi PmenuThumb guifg=".s:red." guibg=".s:shade2
exec "hi Question guifg=".s:shade7." guibg=".s:shade1
exec "hi Search guifg=".s:shade0." guibg=".s:aqua
exec "hi SpecialKey guifg=".s:magenta." guibg=".s:shade0
exec "hi SpellBad guifg=".s:red
exec "hi SpellCap guifg=".s:aqua
exec "hi SpellLocal guifg=".s:yellow
exec "hi SpellRare guifg=".s:orange
exec "hi StatusLine guifg=".s:shade4." guibg=".s:shade1." gui=none cterm=none"
exec "hi TabLine guifg=".s:shade5." guibg=".s:shade1
exec "hi TabLineFill guibg=".s:shade1
exec "hi TabLineSel guifg=".s:shade6." guibg=".s:shade0
exec "hi Title guifg=".s:blue
exec "hi Visual guibg=".s:shade1
exec "hi VisualNOS guifg=".s:red." guibg=".s:shade1
exec "hi WarningMsg guifg=".s:red
exec "hi WildMenu guifg=".s:yellow." guibg=".s:shade1

" NERDTree

exec "hi NERDTreeExecFile guifg=".s:yellow
exec "hi NERDTreeDirSlash guifg=".s:blue
exec "hi NERDTreeCWD guifg=".s:red

""""""""""""
" Clean up "
""""""""""""

unlet s:shade0 s:shade1 s:shade2 s:shade3 s:shade4 s:shade5 s:shade6 s:shade7 s:red s:orange s:aqua s:green s:yellow s:blue s:teal s:magenta

