" Set arrow keys to resize panes
nnoremap <Left> :vertical resize -1<CR>
nnoremap <Right> :vertical resize +1<CR>
nnoremap <Up> :resize -1<CR>
nnoremap <Down> :resize +1<CR>

" Return to the last file opened
nmap <Leader><Leader> <c-^>

" Next/Previous Buffer (Tab)
nnoremap <Tab> :bnext!<CR>
nnoremap <S-Tab> :bprev!<CR><Paste>

" Clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y
nnoremap  <leader>yy  "+yy

nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" Quicker window movement
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-h> <C-w>h
nnoremap <A-l> <C-w>l

" Close individual windows
nnoremap <C-q> <C-w>q

" Visual repeat shortcut
vnoremap . :normal .<CR>

" Neovim settings editing
nmap <silent> <leader>lv :edit ~/.config/nvim/config/loader.vim<CR>
nmap <silent> <leader>bv :edit ~/.config/nvim/config/base.vim<CR>
nmap <silent> <leader>kv :edit ~/.config/nvim/config/keymaps.vim<CR>
nmap <silent> <leader>pv :edit ~/.config/nvim/config/plugins.vim<CR>
nmap <silent> <leader>v :edit ~/.config/nvim/init.vim<CR>

" Neovim settings reload
nmap <silent> <leader>sv :source ~/.config/nvim/init.vim<CR>

