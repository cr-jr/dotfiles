" Set standard file encoding
set encoding=utf8

" No special per file vim override configs
set nomodeline

" Stop word wrapping
set nowrap
  " preserve it for Markdown though
  autocmd FileType markdown setlocal wrap

" Adjust system undo levels
set undolevels=100

" Set tab width and convert tabs to spaces
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

" Don't let Vim hide characters or make loud dings
set conceallevel=1
set noerrorbells

" Set line numbers
set number

" Use search highlighting
set hlsearch

" Make backspace behave
set backspace=indent,eol,start

" Switch syntax highlighting on
syntax on

"Enable file type detection and do language-dependent indenting
filetype plugin indent on

" Allow hidden buffers, don't limit to one file per window/split
set hidden

" Allow search to highlight result
set hlsearch

" Tab settings (hard tabs, width 2)
set tabstop=2
set shiftwidth=2

" Set scroll offset to typical length of small fns
set scrolloff=10

" Automatic indent
set autoindent

" Disable .swp files
set noswapfile

" Make j,k work as expected
nnoremap j gj
nnoremap k gk

" Sane search
nnoremap <expr> n  'Nn'[v:searchforward]
xnoremap <expr> n  'Nn'[v:searchforward]
onoremap <expr> n  'Nn'[v:searchforward]

nnoremap <expr> N  'nN'[v:searchforward]
xnoremap <expr> N  'nN'[v:searchforward]
onoremap <expr> N  'nN'[v:searchforward]

" Save on lost focus
autocmd FocusLost * :wa

" Set a usable history limit
set history=500

" Remap leader to space
let mapleader="\<Space>"

" Disable mouse support
set mouse=r
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" True color terminal
if (has('termguicolors'))
  set termguicolors
end

