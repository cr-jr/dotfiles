" Loads all the startup requirements
if &compatible
  set nocompatible " Be iMproved
endif

call plug#begin('~/.local/share/nvim/plugged')
Plug 'HerringtonDarkholme/yats.vim'
Plug 'Shougo/denite.nvim', {'do': ':UpdateRemotePlugins'}
Plug 'Yggdroot/indentLine'
Plug 'airblade/vim-gitgutter'
Plug 'alvan/vim-closetag'
Plug 'ap/vim-buftabline'
Plug 'chrisbra/Colorizer'
Plug 'https://gitlab.com/dbeniamine/todo.txt-vim.git'
Plug 'editorconfig/editorconfig-vim'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'jonsmithers/vim-html-template-literals'
Plug 'junegunn/goyo.vim'
Plug 'leafgarland/typescript-vim',
Plug 'majutsushi/tagbar' | Plug 'universal-ctags/ctags'
Plug 'metakirby5/codi.vim'
Plug 'mhinz/vim-startify'
Plug 'neoclide/coc-neco' | Plug 'Shougo/neco-vim'
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}
Plug 'nightsense/night-and-day'
Plug 'othree/yajs.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-git'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'vimlab/split-term.vim'
Plug 'w0rp/ale'
Plug 'wakatime/vim-wakatime'
call plug#end()
