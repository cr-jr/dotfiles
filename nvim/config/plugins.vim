" Colorscheme
colorscheme ChronotypeV2MorningEvening
set background=light
let g:lightline = { 'colorscheme': 'ChronotypeV2MorningEveningLightline' }

" ale
let g:ale_linters = {
\   'javascript': ['eslint', 'prettier', 'standard'],
\}
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\}
let g:ale_linters_explicit = 1
let g:ale_fix_on_save = 1

" night-and-day
let g:nd_themes = [
  \ ['sunrise+0', 'ChronotypeV2MorningEvening', 'light', 'ChronotypeV2MorningEveningLightline'],
  \ ['sunrise+1/2', 'ChronotypeV2MiddayMidnight', 'light', 'ChronotypeV2MiddayMidnightLightline'],
  \ ['sunset+0',  'ChronotypeV2MorningEvening', 'dark', 'ChronotypeV2MorningEveningLightline'],
  \ ['sunset+1/2', 'ChronotypeV2MiddayMidnight', 'dark', 'ChronotypeV2MiddayMidnightLightline'],
  \ ]
let g:nd_lightline = 1

let g:nd_latitude = '40'

if strftime("%m") > 2 && strftime("%m") < 10
  let g:nd_timeshift = '90'
else
  let g:nd_timeshift = '30'
endif

" split-term.vim
set splitright
set splitbelow


" codi.vim
let g:codi#width=33.0
let g:codi#rightalign=0
nmap <leader>c :Codi!!<CR>

" goyo.vim
let g:goyo_width=120
nnoremap <silent> <leader>z :Goyo<CR>

" Colorizer
:let g:colorizer_auto_filetype='css,html,javascript,json'

" vim-closetag
let g:closetag_filenames="*.html,*.xhtml,*.phtml,*.jsx"

" editorconfig
let g:EditorConfig_exclude_patterns=['fugitive://.*', 'scp://.*']
let g:EditorConfig_exec_path='~/.editorconfig'

" tagbar
let g:tabar_autofocus=1
nmap <Leader>i :TagbarToggle<CR>

" lightline.vim
set noshowmode

let g:lightline.enable = { 'tabline': 0}

let g:lightline.active = {
    \ 'left': [ [ 'mode', 'paste' ],
    \           [ 'readonly', 'filename', 'modified' ] ],
    \ 'right': [ ['lineinfo'],
    \            [ 'percent' ],
    \            [ 'filetype' ] ] }

let g:lightline.inactive = {
    \ 'left': [ [ 'filename' ] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'percent' ] ] }

" vim-buftabline
let g:buftabline_show=1
let g:buftabline_numbers=2
let g:buftabline_plug_max=7

nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)

" denite.vim

" reset 50% winheight on window resize
augroup deniteresize
  autocmd!
  autocmd VimResized,VimEnter * call denite#custom#option('default',
        \'winheight', winheight(0) / 2)
augroup end

call denite#custom#option('default', {
      \ 'prompt': '❯'
      \ })

call denite#custom#var('file_rec', 'command',
      \ ['rg', '--files', '--glob', '!.git'])
call denite#custom#var('grep', 'command', ['rg'])
call denite#custom#var('grep', 'default_opts',
      \ ['--hidden', '--vimgrep', '--smart-case'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])
call denite#custom#map('insert', '<Esc>', '<denite:enter_mode:normal>',
      \'noremap')
call denite#custom#map('normal', '<Esc>', '<NOP>',
      \'noremap')
call denite#custom#map('insert', '<C-v>', '<denite:do_action:vsplit>',
      \'noremap')
call denite#custom#map('normal', '<C-v>', '<denite:do_action:vsplit>',
      \'noremap')
call denite#custom#map('normal', 'dw', '<denite:delete_word_after_caret>',
\'noremap')

" Files
nnoremap <C-p> :<C-u>Denite file_rec<CR>

" Buffer
nnoremap <leader>s :<C-u>Denite buffer<CR>
nnoremap <leader>S :<C-u>DeniteBufferDir buffer<CR>

" Grepping
nnoremap <leader># :<C-u>DeniteCursorWord grep:. -mode=normal<CR>
nnoremap <leader>/ :<C-u>Denite grep:. -mode=normal<CR>
nnoremap <leader>? :<C-u>DeniteBufferDir grep:. -mode=normal<CR>

" Directories
nnoremap <leader>d :<C-u>DeniteBufferDir file_rec<CR>

" vim-startify
let g:startify_session_dir = '~/.config/nvim/sessions'
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Projects']       },
      \ { 'type': 'dir',       'header': ['   CWD: '. getcwd()] },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ { 'type': 'commands',  'header': ['   Commands']       },
      \ ]
let g:startify_files_number = 7
let g:startify_session_persistence = 1
let g:startify_fortune_use_unicode = 1
let g:startify_session_sort = 1
let g:startify_custom_indices = ['z', 'x', 'c', 'v', 'b', 'n', 'm']

