" init.vim
" =================================================================
" loader.vim: Plugin loader and initial startup requirements
" base.vim: Sane and opinionated base settings to get going
" keymaps.vim: Plugin independent keymappings for productivity
" plugins.vim: Plugin dependent settings and keymaps

source ~/.config/nvim/config/loader.vim
source ~/.config/nvim/config/base.vim
source ~/.config/nvim/config/keymaps.vim
source ~/.config/nvim/config/plugins.vim

