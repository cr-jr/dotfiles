# This file has been auto-generated by i3-config-wizard(1).
# It will not be overwritten, so edit it as you like.
#
# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
#

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:FuraCode NF 10

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# Launch menu
bindsym $mod+Shift+d exec i3-dmenu-desktop

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# Scratchpad keys
bindsym $mod+Shift+Menu move scratchpad
bindsym $mod+Menu scratchpad show

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "Concepts"
set $ws2 "Prototyping"
set $ws3 "Research"
set $ws4 "Assets"
set $ws5 "Dev"
set $ws6 "Builds"
set $ws7 "Documentation"
set $ws8 "Testing"

# Autoswitch workspaces
workspace_auto_back_and_forth yes

# Set workspace output regions
workspace $ws1 output HDMI-0 LVDS
workspace $ws2 output HDMI-0 LVDS
workspace $ws3 output HDMI-0 LVDS
workspace $ws4 output HDMI-0 LVDS
workspace $ws5 output primary
workspace $ws6 output HDMI-0 LVDS
workspace $ws7 output primary
workspace $ws8 output HDMI-0 LVDS

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8; workspace $ws8

# Designate apps to open on specific workspaces
assign [class="^FontBase$"] $ws1
assign [class="^figma-linux$"] $ws1
assign [class="^@meetalva/core$"] $ws2
assign [class="^Firefox Developer Edition$"] $ws3
assign [class="^Gimp$"] $ws4
assign [class="^Inkscape$"] $ws4
assign [class="^Scribus$"] $ws4
assign [class="^kitty$"] $ws5
assign [class="^Typora$"] $ws7
assign [class="^DevDocs$"] $ws7
assign [class="^puppetry$"] $ws8

# Force Alva to tile
for_window [class="^@meetalva/core$"] floating disable


# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Toolset mode
set $toolbox Tools: [f]ontBase [Shift+f]igma [a]lva [d]evdocs [p]uppetry
bindsym $mod+t mode "$toolbox"

mode "$toolbox" {
  bindsym f exec appimage-run ~/Apps/FontBase-2.7.0.AppImage
  bindsym Shift+f exec appimage-run ~/Apps/FigmaUnofficial-0.5.1.AppImage
  bindsym a exec appimage-run ~/Apps/Alva-0.9.1.AppImage
  bindsym d exec appimage-run ~/Apps/DevDocs-0.6.9-x86_64.AppImage
  bindsym p exec appimage-run ~/Apps/puppetry-linux-v1.0.10-x86_64.AppImage

  bindsym Escape mode "default"
  bindsym Return mode "default"
}

# Enable gaps
for_window [class=".*"] border pixel 1

# Smart gaps
smart_gaps on
smart_borders on

# Configure gaps
gaps inner 8

# Polybar init
exec_always --no-startup-id $HOME/.config/polybar/launch.sh

# Nitrogen wallpaper
exec --no-startup-id nitrogen --restore

# Albert
exec --no-startup-id albert

